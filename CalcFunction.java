package test;

import java.util.*;

enum Opt {
	add, substract, multiply, divide
}
public class CalcFunction {
	    private String result;
	    private String left;
	    private String right;
	    private Opt opt;
	    
	    public void calc(String s){
	    	String[] strArr = getOpt(s);
	        if (strArr == null || strArr.length != 2 || getOpt(strArr[0]) != null || getOpt(strArr[1]) != null) {
	        	throw new IllegalArgumentException("Input error!");//格式错误时抛出异常
	        }
	        int[] v = changeToFS(strArr[0], strArr[1]);
	    	switch (this.opt) {
			case add:
				result = simplefraction((v[0]*v[3]+v[1]*v[2]),(v[1]*v[3]));
				break;
			case substract:
				result = simplefraction((v[0]*v[3]-v[1]*v[2]),(v[1]*v[3])); 
				break;
			case multiply:
				result = simplefraction((v[0]*v[2]),(v[1]*v[3]));
				break;
			default:
				result = simplefraction((v[0]*v[3]),(v[1]*v[2]));
			}
	        System.out.println(result);
	    }
	    
    
    public String getResult()
    {
        return result;
    } 
	    
	
    public static int GCD(int m, int n) {//递归法求最大公约数
        if(m % n == 0){
            return n;
        }else{
            while(m != n){
                if(m > n)m = m - n;
                else n = n - m;
            }
        }
        return m;
}
    public static int LCM(int m, int n){//求最小公倍数
        return m * n / GCD(m,n);
    }
    
    static String simplefraction(int m, int n){ //化简分数
    String s;
    int num = GCD(m, n);
            m = m / num;
            n = n / num;
    if (n == 1) {
        s = m + "";
    } else {
        s = m + "" + "/" + n + "";
    }
    return s;
}
	
    /**
     * 拆分式子
     * @param s
     * @return
     */
    private String[] getOpt(String s) {
    	int i = -1;
    	String opt;
    	if(s.indexOf("+")>-1){
    		opt = "[+]";
    		this.opt = Opt.add;
    		i = s.indexOf("+");
    	} else if (s.indexOf("-")>-1){
    		opt = "[-]";
    		this.opt = Opt.substract;
    		i = s.indexOf("-");
    	} else if (s.indexOf("*")>-1){
    		opt = "[*]";
    		this.opt = Opt.multiply;
    		i = s.indexOf("*");
    	} else if (s.indexOf("÷")>-1){
    		opt = "[÷]";
    		this.opt = Opt.substract;
    		i = s.indexOf("÷");
    	} else {
    		return null;
    	}
    	String[] str=s.split(opt);
    	return str;
    }
    
    
    /**
     * 转成分数形式
     * @param left
     * @param right
     * @return
     */
    private int[] changeToFS(String left, String right) {
    	String[] str1=left.split("[/]");
    	String[] str2=right.split("[/]");
    	int[] v = new int[4];
    	v[0] = Integer.parseInt(str1[0]);
    	if (str1.length < 2) {            //防止  1+1/4 的情况
    		v[1] = 1;
    	} else {
    		v[1] = Integer.parseInt(str1[1]);
    	}
    	v[2] = Integer.parseInt(str2[0]);
    	if (str2.length < 2) {
    		v[3] = 1;
    	} else {
    		v[3] = Integer.parseInt(str2[1]);
    	}
    	if(v[1] == 0 || v[3] == 0) {   //分母不为零
    		throw new IllegalArgumentException("Divisor cannot be zero!");//除数为零时抛出异常
    	}
    	return v;
    }
}